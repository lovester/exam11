const mongoose = require('mongoose');
const config = require('./config');

const Category = require('./models/Category');
const Product = require('./models/Product');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const categories = await Category.create(
        {title: 'Cars', description: 'All kinds of transport'},
        {title: 'Property', description: 'All kins of property'},
        {title: 'Services', description: 'All kins of services'},
    );

    await Product.create(
        {
            title: 'Lexus',
            price: 6000,
            description: 'Big and comfortable',
            category: categories[0]._id,
            image: 'car.jpg'
        },
        {
            title: 'House',
            price: 60000,
            description: 'Little house in the country',
            category: categories[1]._id,
            image: 'house.jpg'
        },
        {
            title: 'Accounting reports',
            price: 500,
            description: 'Accounting reports and financial analysis',
            category: categories[2]._id,
            image: 'report.jpg'
        }
    );

    await connection.close();
};

run().catch(error => {
    console.error('Something went wrong', error);
});
