import React, {Component, Fragment} from 'react';
import {Container} from "reactstrap";
import {Route, Switch} from "react-router-dom";

import Layout from "./components/Layout/Layout";
import Products from "./containers/Products/Products";
import NewProduct from "./containers/NewProduct/NewProduct";
import Register from "./containers/Register/Register";

class App extends Component {
    render() {
        return (
            <Fragment>
                <Container style={{marginTop: '20px'}}>
                    <Layout>
                        <Switch>
                            <Route path="/" exact component={Products}/>
                            <Route path="/products/new" exact component={NewProduct} />
                            <Route path="/register" exact component={Register} />
                        </Switch>
                    </Layout>
                </Container>
            </Fragment>
        );
    }
}

export default App;
