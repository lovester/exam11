import React, {Component, Fragment} from 'react';
import {fetchCategories} from "../../store/actions/categoriesActions";
import {connect} from "react-redux";

import {Nav, NavItem, NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

class Categories extends Component {
    componentDidMount() {
        this.props.onFetchCategories();
    }

    render() {
        return (
            <Fragment>
                <Nav vertical>
                    {this.props.categories.map(item => {
                        return (
                            <NavItem>
                                <NavLink tag={RouterNavLink} to={'/categories/' +
                                item._id} exact>
                                    {item.title}
                                </NavLink>
                            </NavItem>
                        )
                    })}
                </Nav>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    categories: state.categories.categories
});

const mapDispatchToProps = dispatch => ({
    onFetchCategories: () => dispatch(fetchCategories())
});

export default connect(mapStateToProps, mapDispatchToProps)(Categories);
