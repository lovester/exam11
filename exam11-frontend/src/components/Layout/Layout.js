import React, {Fragment} from 'react';
import Toolbar from '../UI/Toolbar/Toolbar';
import Categories from "../../containers/Categories/Categories";
import {Container} from "reactstrap";

const Layout = props => (
    <Fragment>
        <Container>
            <Toolbar/>
            <main className="Layout-Content">
                {props.children}
            </main>
            <Categories/>
        </Container>
    </Fragment>
);

export default Layout;