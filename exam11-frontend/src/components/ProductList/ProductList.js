import React from 'react';
import PropTypes from 'prop-types';
import {Card, CardBody, CardTitle, Col, Row} from "reactstrap";
import {Link} from "react-router-dom";

import ProductThumbnail from "../ProductThumbnail/ProductThumbnail";

const ProductList = props => {
    return (
        <Row>
            <Col sm="6">
                <Card body outline color="secondary">
                    <Link style={{textAlign: 'center'}} to={'/products/' + props._id}>
                        <CardTitle tag="h3">{props.title}</CardTitle>
                    </Link>
                    <ProductThumbnail image={props.image}/>
                    <CardBody className="text-center">
                        <strong style={{marginLeft: '10px'}}>
                            {props.price} USD
                        </strong>
                    </CardBody>
                </Card>
            </Col>
        </Row>


    );
};

ProductList.propTypes = {
    image: PropTypes.string,
    _id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
};

export default ProductList;
