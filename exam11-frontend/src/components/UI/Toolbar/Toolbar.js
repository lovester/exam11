import React from "react";
import {
    Nav,
    Navbar,
    NavbarBrand, NavItem, NavLink
} from "reactstrap";

import {NavLink as RouterNavLink} from "react-router-dom";
import RegisteredUserMenu from "./Menus/RegisteredUserMenu";
import AnonymousUserMenu from "./Menus/AnonymousUserMenu";

const Toolbar = ({user}) => {
    return (
        <Navbar color="light" light expand="md">
            <NavbarBrand tag={RouterNavLink} to="/">Home</NavbarBrand>

            <Nav className="ml-auto" navbar>
                <NavItem>
                    <NavLink tag={RouterNavLink} to="/" exact>Products</NavLink>
                </NavItem>
                {user ? <RegisteredUserMenu user={user}/> : <AnonymousUserMenu/>}
            </Nav>
        </Navbar>
    );
};

export default Toolbar;
