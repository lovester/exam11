import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {ConnectedRouter, connectRouter, routerMiddleware} from "connected-react-router";
import {Provider} from "react-redux";
import thunkMiddleware from 'redux-thunk';

import categoriesReducer from './store/reducers/categoriesReducers';
import productsReducer from './store/reducers/productsReducer';
import usersReducer from './store/reducers/userReducers';
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import {createBrowserHistory} from "history";

const history = createBrowserHistory();

const rootReducer = combineReducers({
    router: connectRouter(history),
    categories: categoriesReducer,
    products: productsReducer,
    users: usersReducer
});

const composeEnhancers = window.REDUX_DEVTOOLS_EXTENSION_COMPOSE || compose;

const middleware = [
    thunkMiddleware,
    routerMiddleware(history)
];

const enhancers = composeEnhancers(applyMiddleware(...middleware));

const store = createStore(rootReducer, enhancers);

const app = (
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <App/>
        </ConnectedRouter>
    </Provider>
);



ReactDOM.render(app, document.getElementById('root'));
serviceWorker.unregister();
